import React, {useState} from 'react';
import './Form.css'

const dateOption = {
    hour12: false,
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour:'2-digit',
    minute: '2-digit'
  };

const Form = ({closeModal, editItem, editArtist}) => {
    const {id, name, country, rating, updatedTime, twitter, comments} = editItem;
    const [artistName, setName] = useState(name);
    const [artistRating, setRating] = useState(rating);
    const [artistComments, setComments] = useState(comments);

    function onNameChange(event){
        const value = event.target.value;

        if(value.length <= 50){
            setName(value);
        } 
    }

    function onRatingChange(event){
        setRating(event.target.value);
    }

    function onCommentsChange(event){
        setComments(event.target.value);
    }

    function onSubmitHandler(event){
        event.preventDefault();
        const item = {
            artist_id: id,
            artist_name: artistName,
            artist_country: country,
            artist_rating: artistRating,
            artist_twitter_url: twitter,
            updated_time: updatedTime,
            artist_comments: artistComments
        }
        editArtist(item);
    }

    return(
        <form onSubmit={onSubmitHandler}>
            <h3 className="form-captions">Edit Dialog</h3>
            <p>
                <input className="form-input" type="text" name="artist_id" value={id} disabled/>
            </p>
            <p>
                <input className="form-input" type="text" name="artist_name" value={artistName} onChange={onNameChange}/>
            </p>
            <p>
                <input className="form-input" type="text" name="artist_country" value={country} disabled/>
            </p>
            <p>
                <input className="form-input" type="number" name="artist_rating" min="0" max="100" value={artistRating} onChange={onRatingChange}/>
            </p>
            <p>
                <input className="form-input" type="text" name="artist_upload_time" value={new Date(updatedTime).toLocaleString('en-US', dateOption)} disabled/>
            </p>
            <p>
                <input className="form-input" type="text" name="artist_twitter" value={twitter} disabled/>
            </p>
            <p>
                <textarea className="form-input"  name="artist_comments" value={artistComments} onChange={onCommentsChange}></textarea>
            </p>
            <p className="form-buttons">
                <button className="form-buttons_item" onClick={closeModal}>Cancel</button>
                <button className="form-buttons_item" type="submit">Save</button>
            </p>
        </form>
    )
};

export default Form;