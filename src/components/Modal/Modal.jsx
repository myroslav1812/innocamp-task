import React from 'react';
import Form from './Form/Form';
import './Modal.css'

const Modal = ({closeModal, editItem, editArtist}) => {
    return(
        <>
            <div className="modal"/>
            <div className="form">
                <Form closeModal={closeModal} editItem={editItem} editArtist={editArtist}/>
                <button onClick={closeModal} className="form-close-btn"/>
            </div>

        </>
    )
};

export default Modal;