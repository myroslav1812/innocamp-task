import React from 'react';

const dateOption = {
    hour12: false,
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour:'2-digit',
    minute: '2-digit'
  };

const ArtistItem = ({id, name, country, rating, updatedTime, twitter, comments, openModal}) => {
    function showModal(){
        openModal({id, name, country, rating, updatedTime, twitter, comments})
    }
    return (
        <div className="artists-header artists-item">
            <p className="artists-col" onClick={showModal}>{id}</p>
            <p className="artists-col">{name}</p>
            <p className="artists-col">{country}</p>
            <p className="artists-col">{rating}</p>
            <p className="artists-col">{new Date(updatedTime).toLocaleString('en-US', dateOption)}</p>
        </div>
    );
};

export default ArtistItem;