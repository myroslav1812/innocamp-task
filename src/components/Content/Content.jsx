import React from "react";
import ArtistItem from "./ArtistItem.jsx/ArtistItem";
import "./Content.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown, faArrowUp } from "@fortawesome/free-solid-svg-icons";

const Content = ({
  artists,
  openModal,
  sortName,
  sortNameDir,
  sortRating,
  sortRatingDir,
  sortDate,
  sortDateDir
}) => {

  const dArr = <FontAwesomeIcon icon={faArrowDown} />;
  const uArr = <FontAwesomeIcon icon={faArrowUp} />;
 
    if(artists === undefined){
      return <div className="no-cards"> Loading... </div>;
    }


    if (artists.length === null) {
        return <div className="no-cards"> No cards </div>;
    }

  

  const artistsArray = artists.map(
    ({
      artist_id,
      artist_name,
      artist_country,
      artist_rating,
      updated_time,
      artist_twitter_url,
      artist_comments
    }) => (
      <ArtistItem
        key={artist_id}
        id={artist_id}
        name={artist_name}
        country={artist_country}
        rating={artist_rating}
        updatedTime={updated_time}
        twitter={artist_twitter_url}
        comments={artist_comments}
        openModal={openModal}
      />
    )
  );

  return (
    <section className="artist">
      <div className="artists-wrapper">
        <div className="artists-header artists-item">
          <p className="artists-col">Id</p>
          <p className="artists-col" onClick={sortName}>
            Name {sortNameDir == null ? "" : sortNameDir ? dArr : uArr}
          </p>
          <p className="artists-col">Country</p>
          <p className="artists-col" onClick={sortRating}>
            Rating {sortRatingDir == null ? "" : sortRatingDir ? dArr : uArr}
          </p>
          <p className="artists-col" onClick={sortDate}>
            Uploaded at {sortDateDir == null ? "" : sortDateDir ? dArr : uArr}
          </p>
        </div>
        <div className="artists-content">{artistsArray}</div>
      </div>
    </section>
  );
};

export default Content;
