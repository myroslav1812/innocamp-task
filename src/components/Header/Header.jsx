import React from 'react';
import './Header.css'

const Header = ({searchValue, onSearchChange}) =>{
    return(
        <header>
            <p className="input-wrapper">
                <input type="text" className="search-input" placeholder="Search by name..." value={searchValue} onChange={onSearchChange}/>
            </p>
        </header>
    )
}

export default Header;