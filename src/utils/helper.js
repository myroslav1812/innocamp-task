import data from '../data/MusicArtists.json';

function setItemsToStorage(items){
    localStorage.setItem('artists', JSON.stringify(items));
}

export function getArtists() {
    if(!localStorage['artists']){
        const artists = data.artist_list.map(item => ({...item.artist, artist_comments: ''}));
        setItemsToStorage(artists);
    }
    return JSON.parse(localStorage.getItem('artists'));
}



export function editItemInArray(artists, item){
    const newArtists = artists.map(art => art.artist_id === item.artist_id ? item : art);
    setItemsToStorage(newArtists);
    return newArtists;
}

export function searchInArray(artists, value){
    value = value.toLowerCase();
    return artists ? artists.filter(art => art.artist_name.toLowerCase().includes(value)) : artists;
}

export function sortByProp(artists, prop, direct){
    const sorted = artists.sort((a, b) => {
        if(a[prop] < b[prop])
            return -1;
        if(a[prop] > b[prop])
            return 1;
        return 0; 
    });
    return !!direct ? sorted : sorted.reverse();
}

