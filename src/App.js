import React, {useState, useEffect} from 'react';
import './App.css';
import Header from './components/Header/Header'
import Content from './components/Content/Content';
import {getArtists, editItemInArray, searchInArray, sortByProp} from './utils/helper'
import Modal from './components/Modal/Modal';

function App() {
  const [artists, setArtists] = useState(undefined);
  const [sortByName, setSortByName] = useState(null);
  const [sortByRating, setSortByRating] = useState(null);
  const [sortByDate, setSortByDate] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [editItem, setEditItem] = useState(null);
  const [SearchValue, setSearchValue] = useState('');

  useEffect(()=>{setTimeout(() => setArtists(getArtists), 1500)}, []);

  function sortName(){
    setSortByName(!sortByName);
    setSortByRating(null);
    setSortByDate(null);
    setArtists(sortByProp(artists, 'artist_name', sortByName));
  }

  function sortRating(){
    setSortByRating(!sortByRating);
    setSortByDate(null);
    setSortByName(null);
    setArtists(sortByProp(artists, 'artist_rating', sortByRating));
  }

  function sortDate(){
    setSortByDate(!sortByDate);
    setSortByName(null);
    setSortByRating(null);
    setArtists(sortByProp(artists, 'updated_time', sortByDate));
  }


  function closeModal(){
    setShowModal(false);
    setEditItem(null);
  }

  function openModal(item){
    setEditItem(item)
    setShowModal(true);
  }

  function editArtist(item){
    setArtists(editItemInArray(artists, item));
    setShowModal(false);
  }

  function onSearchFieldChange(event){
    setSearchValue(event.target.value);
  }

  const filteredArtists = searchInArray(artists, SearchValue);

  return (
    <div className="main-wrapper">
      <Header onSearchChange={onSearchFieldChange}/>
      <Content artists={filteredArtists} 
        openModal={openModal} 
        sortDate={sortDate} 
        sortDateDir={sortByDate}
        sortRating={sortRating} 
        sortRatingDir={sortByRating}
        sortName={sortName} 
        sortNameDir={sortByName}/>
      {showModal && <Modal closeModal={closeModal} editItem={editItem} editArtist={editArtist}/>}
    </div>
  );
}

export default App;
